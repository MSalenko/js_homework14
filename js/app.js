const themeBtn = document.getElementById('themeBtn');
const theme = document.getElementById('theme');

function defaultTheme() {
  const previousTheme = localStorage.getItem('theme');
  if (previousTheme === null) {
    applyTheme('light');
  } else {
    applyTheme(previousTheme);
  }
}

function changeTheme() {
  if (theme.getAttribute('href') === './css/dark-theme.css') {
    applyTheme('light');
  } else {
    applyTheme('dark');
  }
}

function applyTheme(themeName) {
  theme.setAttribute('href', `./css/${themeName}-theme.css`);
  localStorage.setItem('theme', themeName);
}

defaultTheme();
themeBtn.addEventListener('click', changeTheme);
